import helpers
import json
import logging
import os
import requests
from string import upper
from sys import argv
import git_tools

# Jira setup
api_token = os.environ['JIRA_CREDENTIALS']
api_url_base = ''
headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic {0}'.format(api_token)
}


def get_jira_stories(lanes):
    params = {
        'jql': 'project=XXX&status in ("{0}")'.format('", "'.join(lanes)),
        'maxResults': '100'
    }
    api_url = '{0}search'.format(api_url_base)

    # Request
    response = requests.get(api_url, headers=headers, params=params)
    logging.debug('Response: {0}'.format(response.content))

    if response.status_code == 200:
        if response.content is not None:
            return response.content
        else:
            logging.error('Something went wrong while calling Jira!')
            exit(1)
    elif response.status_code == 401:
        logging.error('Jira authentication failure')
    else:
        logging.warning('[!] HTTP {0} calling [{1}]'.format(response.status_code, api_url))
        return None


def change_jira_swimlane(issue, lane):
    data = {
        'transition': {'id': '{0}'.format(resolve_swimlane_code(upper(lane)))}
    }
    api_url = '{0}issue/{1}/transitions'.format(api_url_base, issue)

    # Request
    response = requests.post(api_url, headers=headers, data=json.dumps(data))
    print(response.content)
    if response.status_code == 204:
        if response.content is not None:
            logging.info('Issue {0} moved to {1}'.format(issue, lane))
        else:
            logging.error('Something went wrong while moving issue {0} to {1}'.format(issue, lane))
            exit(1)
    elif response.status_code == 401:
        logging.error('Jira authentication failure')
    else:
        logging.warning('[!] HTTP {0} calling [{1}]'.format(response.status_code, api_url))


def empty_jira_swimlane(inlane, outlane):
    issue_list = simple_issue_list([inlane], internal=True)
    for issue in issue_list:
        change_jira_swimlane(issue, outlane)


def simple_issue_list(lanes, internal=False):
    return_str = ''
    issue_list = []

    # Call Jira
    # with open('sample_data.json') as f:  # For reading from file
    issues = json.loads(get_jira_stories(lanes))['issues']

    for issue in issues:
        issue_list.append(str(issue['key']))

    if internal:
        return issue_list
    else:
        if issue_list:
            for item in issue_list[:-1]:
                return_str += item + ' '
            return_str += issue_list[-1]

        logging.info('Issue list: {0}'.format(issue_list))
        print(return_str)


def comment_on_story(story_number, comment):
    data = {
        'body': '{}'.format(comment)
    }
    api_url = '{0}issue/{1}/comment'.format(api_url_base, story_number)

    # Request
    response = requests.post(api_url, headers=headers, data=json.dumps(data))
    print(response.content)
    if response.status_code == 201:
        if response.content is not None:
            logging.info('Comment created on {}'.format(story_number))
        else:
            logging.error('Something went wrong while commenting on issue'.format(story_number))
            exit(1)
    elif response.status_code == 401:
        logging.error('Jira authentication failure')
    else:
        logging.warning('[!] HTTP {0} calling [{1}]'.format(response.status_code, api_url))


def sorted_issue_list(lanes):
    issue_list = []
    processed_issues = []
    processed_twice = []

    # Call Jira
    # with open('sample_data.json') as f:  # For reading from file
    issues = json.loads(get_jira_stories(lanes))['issues']
    no_branches = []

    for issue in issues:
        # Check custom Jira field for 'create git branch' == True
        if issue['fields']['customfield_10846'] is not None:
            if issue['fields']['customfield_10846'][0]['value'] == "True":
                issue_list.append({'issue': str(issue['key']), 'lane': str(issue['fields']['status']['name']), })
        else:
            no_branches.append(issue['key'])
    if no_branches:
        logging.info(' No associated branches: {}'.format(' '.join(no_branches)))

    logging.info(' Issue list: {0}'.format(issue_list))

    # Sorting
    for issue in issue_list:
        issue.update({'timestamp': str(git_tools.get_last_commit_time(issue['issue']))})
        processed_issues.append(issue)

    # Remove failures
    processed_issues[:] = [x for x in processed_issues if
                           ((x['timestamp'] is not None) and (x['timestamp'] != 'None'))]

    # Sort by earliest date/time
    processed_issues_sorted = sorted(processed_issues, key=lambda i: i['timestamp'], reverse=False)
    for lane in lanes:
        for issue in processed_issues_sorted:
            if issue['lane'] == lane:
                processed_twice.append(issue['issue'])

    # Dump as string for unix arr
    print(' '.join(processed_twice))


def get_issues(mode, env):
    simple_lanes = ordered_lanes = []

    if env == 'dev':  # To build DEV
        simple_lanes = ['Development Complete']
        ordered_lanes = ['UAT Complete', 'UAT (Alpha)', 'QA Complete', 'In QA', 'Ready for QA', 'Env Dev',
                         'Automation', 'Development Complete']
    elif env == 'test':  # To build TEST
        simple_lanes = ['Ready for QA']
        ordered_lanes = ['UAT Complete', 'UAT (Alpha)', 'QA Complete', 'In QA', 'Ready for QA', 'Env Dev',
                         'Automation']
    elif env == 'stage':  # To build STAGE
        simple_lanes = ['Automation']
        ordered_lanes = ['UAT Complete', 'UAT (Alpha)', 'QA Complete', 'In QA', 'Ready for QA', 'Env Dev']
    else:
        print('An error has occurred!')
        exit(1)

    # Main call
    if mode == 'simple':
        simple_issue_list(simple_lanes)
    elif mode == 'ordered':
        sorted_issue_list(ordered_lanes)


def resolve_swimlane_code(lane):
    if lane == 'TO DO':
        return '11'
    elif lane == 'IN DEVELOPMENT':
        return '31'
    elif lane == 'IN CODE REVIEW':
        return '31'
    elif lane == 'DEVELOPMENT COMPLETE':
        return '41'
    elif lane == 'CODE REVIEW APPROVED':
        return '201'  # Refers to the code review transition and not the lane itself
    elif lane == 'AUTOMATE':
        return '221'  # Refers to the transition and not the lane itself
    elif lane == 'AUTOMATION':
        return '16'
    elif lane == 'ENV DEV':
        return '141'
    elif lane == 'READY FOR QA':
        return '51'
    elif lane == 'IN QA':
        return '61'
    elif lane == 'QA COMPLETE':
        return '71'
    elif lane == 'UAT (ALPHA)':
        return '81'
    elif lane == 'UAT COMPLETE':
        return '111'
    elif lane == 'DONE':
        return '91'
    else:
        logging.error('Invalid swimlane: {}'.format(lane))
        exit(1)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    if len(argv) < 2:
        helpers.jira_help()
        exit(1)

    if argv[1] == 'get_issues':
        if (len(argv) is not 4) or \
                ((argv[2] != 'simple') and (argv[2] != 'ordered') or
                 ((argv[3] != 'dev') and (argv[3] != 'test') and (argv[3] != 'stage'))):
            helpers.jira_get_issues_help()
            exit(1)
        else:
            get_issues(str(argv[2]), str(argv[3]))

    elif argv[1] == 'change_lane':
        if len(argv) is not 4:
            helpers.jira_change_lane_help()
            exit(1)
        else:
            change_jira_swimlane(str(argv[2]), str(argv[3]))

    elif argv[1] == 'empty_lane':
        if len(argv) is not 4:
            helpers.jira_empty_lane_help()
            exit(1)
        else:
            empty_jira_swimlane(str(argv[2]), str(argv[3]))

    elif argv[1] == 'comment':
        if len(argv) is not 4:
            helpers.jira_empty_lane_help()
            exit(1)
        else:
            comment_on_story(str(argv[2]), str(argv[3]))
    else:
        helpers.jira_help()
