import json
import logging
import os
import sys

import requests

import helpers
import jira_tools
from local_utils import datetime_converter

api_token = os.environ['GIT_API_TOKEN']
api_token = api_token
api_url_base = 'https://api.github.com/'
company = ''
repo = ''


def get_last_commit_time(branchname):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': 'token {}'.format(api_token)
    }
    api_url = '{0}repos/{1}/{2}/branches/{3}'.format(api_url_base, company, repo, branchname)

    # Request
    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        if response.content is not None:
            repo_info = json.loads(response.content)
            last_commit_date = repo_info['commit']['commit']['author']['date']

            return datetime_converter(last_commit_date)
        else:
            logging.warning('Repo not found!')
            return None
    else:
        logging.warning('[!] HTTP {0} calling [{1}]'.format(response.status_code, api_url))
        return None


def create_branch(env, branchname):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': 'token {}'.format(api_token)
    }
    api_url = '{0}repos/{1}/{2}/branches/{3}'.format(api_url_base, company, repo, branchname)

    # Request
    response = requests.get(api_url, headers=headers)

    # Branch found
    if response.status_code == 200:
        if response.content is not None:
            logging.warning('Branch {} already exists'.format(branchname))
        else:
            logging.warning('Branch {} was found, but did not return info correctly'.format(branchname))
            return None
    # Branch not found
    else:
        # Get last commit on dev to branch from
        last_commit_api_url = '{0}repos/{1}/{2}/commits/{3}'.format(api_url_base, company, repo, env)
        response = requests.get(last_commit_api_url, headers=headers)
        # Branch creation payload
        data = {
            'ref': "refs/heads/{}".format(branchname),
            'sha': "{}".format(json.loads(response.content)['sha'])
        }

        post_api_url = '{0}repos/{1}/{2}/git/refs'.format(api_url_base, company, repo)
        logging.info('Branch {} does not exist... creating'.format(branchname))
        response = requests.post(post_api_url, headers=headers, data=json.dumps(data))

        if response.status_code == 201:
            logging.info("Branch {} created".format(branchname))
            # Tell Jira
            comment = 'To check out your new branch, run this inside your repo:\n{{code:Shell}}git fetch && git checkout {0}{{code}}'.format(branchname)
            jira_tools.comment_on_story(branchname, comment)
        else:
            logging.warning("Something went wrong during creation of branch {}".format(branchname))
        return None


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    if len(sys.argv) < 2:
        helpers.git_help()
        exit(1)

    if sys.argv[1] == 'create_branch':
        if len(sys.argv) is not 4:
            helpers.git_create_branch_help()
            exit(1)
        else:
            create_branch(str(sys.argv[2]), str(sys.argv[3]))
    else:
        helpers.git_help()
