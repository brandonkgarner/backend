def jira_help():
    print(
        '''
 *  This script requires a function argument
 *  ---------------------------------------------
 *  |   Functions:              get_issues      |
 *  |                           change_lane     |
 *  ---------------------------------------------
 *  ex: python get_issue_list.py simple dev
        '''
    )


def jira_get_issues_help():
    print(
        '''
 *  This script requires 2 arguments, mode and environment
 *  ---------------------------------------------
 *  |   Modes:                    simple        |
 *  |                             ordered       |
 *  ---------------------------------------------
 *  |   Environments:             dev           |
 *  |                             test          |
 *  |                             stage         |
 *  ---------------------------------------------
 *  ex: python get_issue_list.py simple dev
        '''
    )


def jira_change_lane_help():
    print(
        '''
 *  This script requires jira-issue-key and swimlane arguments
 *  ex: python jira_tools.py change_lane XXX-333 "IN DEVELOPMENT"
        '''
    )


def jira_empty_lane_help():
    print(
        '''
 *  This script requires 2 swimlanes as arguments, (1) lane to empty, (2) destination lane
 *  ---------------------------------------------
 *  |   lanes:          'TO DO'                 |
 *  |                   'IN DEVELOPMENT'        |
 *  |                   'IN CODE REVIEW'        |
 *  |                   'DEVELOPMENT COMPLETE'  |
 *  |                   'AUTOMATION'            |
 *  |                   'ENV HOLD'              |
 *  |                   'READY FOR QA'          |
 *  |                   'IN QA'                 |
 *  |                   'QA COMPLETE'           |
 *  |                   'UAT (ALPHA)'           |
 *  |                   'UAT COMPLETE'          |
 *  |                   'DONE'                  |
 *  ---------------------------------------------
 *  ex: python jira_tools.py empty_lane "DEV COMPLETE" "READY FOR QA"
        '''
    )


def git_help():
    print(
        '''
 *  This script requires a function argument
 *  ---------------------------------------------
 *  |   Functions:          create_branch       |
 *  ---------------------------------------------
 *  ex: python git_tools.py create_branch
        '''
    )


def git_create_branch_help():
    print(
        '''
 *  This script requires environment and branchname arguments
 *  ex: python git_tools.py create_branch prod XXX-333
        '''
    )
