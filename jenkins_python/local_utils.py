from time import strptime, mktime


def datetime_converter(datetime_string):
    # Convert to datetime format
    target_timestamp = strptime(datetime_string, '%Y-%m-%dT%H:%M:%SZ')

    # Epoch
    mktime_epoch = mktime(target_timestamp)
    return int(mktime_epoch)
